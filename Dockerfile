FROM alpine:3.12

# application dependencies, we add them here to support better layer caching.
# hadolint ignore=DL3008
RUN apk add --no-cache \
  ca-certificates \
  curl \
  jq \
  libintl \
  libmediainfo \
  sqlite-libs \
  icu-libs

ARG TARGETARCH
ARG RADARR_BRANCH="nightly"
ARG CREATED
ARG SOURCE
ARG REVISION

# container metadata
LABEL org.opencontainers.image.title="mcf.io Radarr - A fork of Sonarr to work with movies à la Couchpotato." \
  org.opencontainers.image.url="https://radarr.video/" \
  org.opencontainers.image.created="${CREATED}" \
  org.opencontainers.image.source="${SOURCE}" \
  org.opencontainers.image.revision="${REVISION}"

ENV APP_PATH /opt/radarr
ENV CONFIG_PATH /config

SHELL ["/bin/sh", "-o", "pipefail", "-c"]
RUN mkdir -p "${APP_PATH}/bin" && \
  if [[ "${TARGETARCH}" == "amd64" ]]; then TARGETARCH=x64; fi && \
  [[ -z ${RADARR_RELEASE} ]] && RADARR_RELEASE="$(curl -sX GET https://radarr.servarr.com/v1/update/${RADARR_BRANCH}/changes?os=linuxmusl | jq -r '.[0].version')" && \
  curl -fsSL "https://radarr.servarr.com/v1/update/${RADARR_BRANCH}/updatefile?version=${RADARR_RELEASE}&os=linuxmusl&runtime=netcore&arch=${TARGETARCH}" | tar xzf - -C "${APP_PATH}/bin" --strip-components=1 && \
  printf "UpdateMethod=docker\nBranch=${RADARR_BRANCH}\nPackageVersion=%s-%s\nPackageAuthor=mcf.io" "${RADARR_RELEASE}" "${REVISION}" > "${APP_PATH}/package_info" && \
  rm -rf "${APP_PATH}/bin/Radarr.Update" && \
  chmod -R u=rwX,go=rX "${APP_PATH}"

ENTRYPOINT [ "/opt/radarr/bin/Radarr" ]
CMD [ "-nobrowser", "-data=/config" ]
EXPOSE 7878
