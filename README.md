# mcf.io/docker-radarr

[Radarr](https://radarr.video/) A fork of [Sonarr](https://sonarr.tv) to work with movies à la Couchpotato.

## Usage

```shell
docker create \
  --name radarr \
  -p 7878:7878 \
  -e PUID=<UID> -e PGID=<GID> \
  -v /etc/localtime:/etc/localtime \
  -v </path/to/appdata>:/config \
  -v </path/to/tvseries>:/movies \
  -v </path/to/downloads>:/downloads \
  mcfio/radarr
```

## Parameters

Docker parameters are split into two components, separated by a colon, the left size represents the host and the right the container.  Example, the -p 8989:8989 parameter identifies the port mapping from host to container.

* `-p 7878:7878` - mapping host:container port for Radarr web interface
* `-v </path/to/appdata>:/config` - Radarr config and database directory
* `-v </path/to/movies>:/movies` - host volume mapping for Movie library
* `-v </path/to/downloads>:/downloads` - host volume mapping for downloads

The container is based on the [mono:5.18-slim](https://hub.docker.com/_/mono/) image, which is debian stretch based, to gain access to the shell execute `docker exec -it radarr bash`

### User / Group IDs

At times, the host volume permissions may not match those of IDs within the running container.  To resolve this the container allows specifying the `PUID` and `PGID`.  Ensure the host volume permissions match those of the container.

For example, either create or use an existing user on the host system and use the values for uid and gid.

```shell
$ id <docker user>
uid=1001(dockeruser) gid=1001(dockeruser) groups=1001(dockeruser)
```

Note: For added security this container supports the use of the `--user` flag both on command line and in `docker-compose` yaml.  The caveat is that ownership of the volume mounts must be set prior to starting the container and should match the values provided to `--user`. 

## Info

You can monitor the logs for the container using `docker logs -f radarr`.

